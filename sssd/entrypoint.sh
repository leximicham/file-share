#!/bin/bash

sed -i "s/SSSD_PASSWORD/$SSSD_PASSWORD/" /etc/sssd/sssd.conf
mkdir -p /var/lib/sss/pipes/private

/usr/sbin/sssd -i --logger=stderr
