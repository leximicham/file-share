#!/bin/bash

if [[ ! -f /run/smb-setup-done ]]; then
  for mount in $(ls -1 /mnt); do
  echo "
  [$mount]
          # This share requires authentication to access
          path = /mnt/$mount
          read only = no
          inherit permissions = yes
          force create mode = 0660
          force directory mode = 2770" | tee -a /etc/samba/smb.conf
  done

  smbpasswd -w ${SSSD_PASSWORD}
  touch /run/smb-setup-done
fi

smbd -F
